from flask import g
from flask_appbuilder.security.views import AuthOAuthView as DefaultAuthOAuthView, UserInfoEditView
from superset.users_model import MyUser
from superset.users_view import MyUserModelView

class MyUserInfoEditView(UserInfoEditView):
	def form_get(self, form):
		item = self.appbuilder.sm.get_user_by_id(g.user.id)
		# fills the form generic solution
		for key, value in form.data.items():
			if key == 'csrf_token': continue
			form_field = getattr(form, key)
			form_field.data = getattr(item, key)
			
class CustomSecurityManager(SecurityManager):
	userinfoeditview = MyUserInfoEditView
	user_model = MyUser
	useroauthmodelview = MyUserModelView
	
appbuilder = AppBuilder(
	app, db.session,
	base_template = 'superset/base.html',
	indexview = MyIndexView,
	security_manager_class = CustomSecurityManager
)

sm.get_session_close()
