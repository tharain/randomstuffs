# Introduction

This script can be used to update function and coverage to lumos users.

Users of this script should provide a list of user email, country and team but
there is a flexibility in including other columns inside as well (:

Just download from the google excel sheet and place the file inside this folder
and you are good to go.

The sample csv is in the data_sample.csv attached.

Basic usage:

```
$ python main.py -h
usage: main.py [-h] [-u USER] [-p PASSWORD] [-d DATABASE]
	       [-t HOST] [-i INPUT_CSV]

Update lumos users' function and coverage into database but not overriding
current datas.

optional arguments:
	-h, --help		show this help message and exit
	-u USER, --user USER	the username to connect postgresql
	-p PASSWORD, --password PASSWORD
				the password to connect postgresql
	-d DATABASE, --database DATABASE
				the postgresql database to connect
	-t HOST, --host HOST 	the host of postgresql
	-i INPUT_CSV, --input-csv INPUT_CSV
				the path to user list in csv format

$ python main.py -i data_downloaded.csv -u lumos -t localhost \
-d lumos -p lumos


