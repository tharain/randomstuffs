#-*- coding: utf-8 -*-
import locale

locale.setlocale(locale.LC_ALL, '')

def compare(str1, str2):
	return locale.strcoll(str1, str2)

def swap(list, index1, index2):
	temp = list[index1]
	list[index1] = list[index2]
	list[index2] = temp

def sort(list):
	total = len(list)
	for i in range(0, total):
		swaps = 0
		for j in range(0, total):
			swaps = 0
			for j in range(0, (total - i - 1)):
				if (compare(list[j], list[j+1]) > 0):
					swap(list, j, j+1)
					swaps = swaps + 1
			if(swaps == 0):
				break
	return list


