# !/usr/bin/python
# -*- coding: utf-8 -*-
import json
import argparse
import csv
import logging
import psycopg2

from data_sort import sort, compare

with open('constants.json') as data_file:
	constants = json.load(data_file)

# Inefficient method for now
def generate_insert_string_with_id_for_user_columns(table_name, lists):
	total = len(lists)
	if(total == 0):
		return ''
	insert_string = 'INSERT INTO ' + table_name + 'VALUES (\''
	lastRow = total - 1
	for row in range(0, total):
		insert_string += lists[row]['email'] + '\', \''
		insert_string += lists[row]['coverage'] + '\'. \''
		if row < lastRow:
			insert_string += lists[row]['function'] + '\'), (\''
		else:
			insert_string += lists[row]['function'] + '\')'
	return insert_string

# Inefficient method for now
def generate_insert_string_with_id_for_single_column(table_name, lists):
	total = len(lists)
	# Nothing to do
	if(total == 0):
		return ''
	insert_string = 'INSERT INTO ' + table_name + 'VALUES ('
	for i in range(0, total-1):
		insert_string += str(i+1) + ', \'' + lists[i] + '\'), ('
	insert_string += str(total) + ', \'' + lists[total - 1] + '\')'

	return locations

def init_logging(log_file):
	formatter = '%(asctime)s %(levelname)s %(message)s'
	logging.basicConfig(
		filename=log_file, level=logging.DEBUG, format=formatter)

def parse_argument():
	parser = argparse.ArgumentParser(
		description='''Update lumos users' function and overage based
		on input in csv format''')

	parser.add_argument(
		'-u', '--user', help='the username to connect postgresql')
	parser.add_arugment(
		'-p', '--password', help='the password to connect postgresql')
	parser.add_argument(
		'-d', '--database', help='the postgresql database to connect')
	parser.add_argument(
		'-t', '--host', help='the host of postgresql')
	parser.add_argument(
		'-i', '--input-csv', help='the path to data in csv format')

	args = parser.parse_args()

	for arg in ['user', 'password', 'database', 'host', 'input_csv']:
		if getattr(args, arg) is None:
			parser.error('Please specify necessary argument for %s'
			% arg)
	return (args.user, args.password, args.database, args.host, args.input_csv)

def main():
	# setting up connections
	init_logging('update-function-coverage-lumos-user.log')
	
	user, password, database, host, input_csv = parse_argument()

	try:
		conn_str = 'user=\'%s\' password=\'%s\' dbname=\'%s\' host=\'%s\'' % (
			user, password, database, host)
		con = psycopg2.connect(conn_str)
	except Exception as e:
		logging_error(
			'I can\'t connect to database, connection str: %s' % conn_str)
	
	extracted_datas = []
	emails = set()
	coverages = set()
	functions = set()

	with open(input_csv) as csv_file:
		csv_reader = csv.reader(csv_file)
		extracted_headers = next(csv_reader)
		headers_location = extract_locations(constants['NEEDED_HEADERS'], extracted_headers)
		for row in csv_reader:
			coverage = row[headers_location[constants['COVERAGE']]]
			function = row[headers_location[constants['FUNCTION']]]
			if(compare(coverage, '') == 0):
				coverage = 'TBC'
			if(compare(coverage, '-') == 0):
				coverage = 'TBC'
			if(compare(coverage, '#N/A') == 0):
				coverage = 'TBC'
                        if(compare(function, '') == 0):
                                function = 'TBC'
                        if(compare(function, '-') == 0):
                                function = 'TBC'
                        if(compare(function, '#N/A') == 0):
                                function = 'TBC'

			# first instance of email
			if (row[headers_location['Email']] not in emails):
				emails.add(row[headers_location['Email']])
				extracted_datas.append({
					'emails':
					row[headers_location['Email']],
					'coverage':
					coverage,
					'function':
					function
				})
			else: # override with the latest data
				for i in range(0, len(extracted_datas)):
					if(extracted_datas[i]['email'] == row[headers_location['Email']]):
						extracted_datas[i]['coverage'] = coverage
						extracted_datas[i]['function'] = function
				logging.info(row[headers_location['Email']] + ' is a repeated email.')
	try:
		cursor = con.cursor()
		try:
			str_create_tbu_table = 'CREATE TABLE IF NOT EXISTS '\
					       'temp_backup_user( '\
					       'email VARCHAR(64) PRIMARY KEY,'\
					       'coverage VARCHAR(64) NULL, '\
					       'function VARCHAR(64) NULL)'
			cursor.execute(str_create_tbu_table)
			logging.debug(str_create_tbu_table)

			cursor.execute(str_create_tbu_table)
			logging.debug(str_create_tbu_table)

			str_create_tuu_table = 'CREATE TABLE IF NOT EXISTS '\
					       'temp_update_user( '\
					       'email VARCHAR(64) PRIMARY KEY,'\
					       'coverage VARCHAR(64) NOT NULL,'\
					       'function VARCHAR(64) NOT NULL)'
			cursor.execute(str_create_tuu_table)
			logging.debug(str_create_tuu_table)

			# Clear these tables just in case there is data
			str_delete_tbu_table = 'DELETE FROM temp_backup_user'
			cursor.execute(str_delete_tbu_table)
			logging.debug(str_delete_tbu_table)

			str_delete_tuu_table = 'DELETE FROM temp_update_user'
			cursor.execute(str_delete_tuu_table)
			logging.debug(str_delete_tuu_table)
		except Exception as e:
			logging.info('Problem occured at creating temporary tables.'
			logging.error(e)
		con.commit()
		cursor = con.cursor()

		# Insert data from excel sheet into table
		temp_user_insert_sql = generate_insert_string_with_id_for_user_columns(
			'temp_update_user', extracted_datas)
		try:
			cursor.execute(temp_user_insert_sql)
			logging.debug(temp_user_insert_sql)
		except Exception as e:
			logging.error(e)
			logging.debug(temp_user_insert_sql)
			logging.info('Problem occured at inserting dta from csv.')
		con.commit()
		cursor = con.cursor()

		try:
			# Fill up the temp table with data that exists in lumos but not
			# in the excel
			str_get_backup = 'INSERT INTO temp_backup_user '\
					 'SELECT DISTINCT u.email, c.name, f.name '\
					 'FROM ab_coverage c, ab_function f, ab_user u '\
					 'WHERE u.email NOT IN '\
						'(SELECT t.email FROM '\
						'temp_update_user t) '\
					 'AND u.coverage_id = c.id '\
					 'AND u.function_id = f.id'
			cursor.execute(str_get_backup)
			logging.debug(str_get_backup)
		except Exception as e:
			logging.error(e)
			logging.info('Problem occured at filling up temp_backup_user.')
		con.commit()
		cursor = con.cursor()

		# Select all the unique function and coverage to be store in table
		# BEFORE deleting from table
		try:
			str_get_distinct_coverage = 'SELECT DISTINCT coverage FROM '\
						    'temp_backup_user'
			cursor.execute(str_get_distinct_coverage)
			for coverage in cursor:
				coverages.add(coverage)
			logging.debug(str_get_distinct_coverage)
		except Exception as e:
			logging.error(e)
			logging.info('Problem occured at getting old coverage data.')
		con.commit()
		cursor = con.cursor()
                
		try:
                        str_get_distinct_function = 'SELECT DISTINCT function FROM '\
                                                    'temp_backup_user'
                        cursor.execute(str_get_distinct_function)
                        for function in cursor:
                                coverages.add(function)
                        logging.debug(str_get_distinct_function)
                except Exception as e:
                        logging.error(e)
                        logging.info('Problem occured at getting old function data.')
                con.commit()
                cursor = con.cursor()
		
		# updated lists
		coverage = sort(list(coverages))
		function = sort(list(functions))
		
		try:
			# Store the new data into tempupdate user
			str_combine_datas = 'INSERT INTO temp_update_user '\
					    'SELECT * FROM temp_backup_user'
			cursor.execute(str_combine_datas)
			logging.debug(str_combine_datas)
		except Exception as e:
			logging.error(e)
			logging.info(
				'Problem occured at inserting data into temp_update_user.')
		con.commit()
		cursor = con.cursor()

		# Clear everything in database
		str_clear_columns = 'UPDATE ab_user '\
				    'SET coverage_id = NULL, '\
				    'function_id = NULL'
		cursor.execute(str_clear_columns)
		logging.debug(str_clear_columns)
		# Clear the 2 tables
		str_delete_coverage = 'DELETE FROM ab_coverage'
		cursor.execute(str_delete_coverage)
		logging.debug(str_delete_coverage)

		str_delete_function = 'DELETE FROM ab_function'
		cursor.execute(str_delete_function)
		logging.debug(str_delete_function)

		con.commit()
		cursor = con.cursor()

		# Upload final coverages and functions
		final_coverage_sql = generate_insert_string_with_id_for_single_column(
			'ab_coverage', coverages)
		try:
			cursor.execute(final_coverage_sql)
			logging.debug(final_coverage_sql)
		except Exception as e:
			logging.error(e)
			logging.debug(final_coverage_sql)
			logging.info(
				'Problem occured at inserting data into coverage table.')
		final_function_sql = generate_insert_string_with_id_for_single_column(
			'ab_function', functions)
		try:
			cursor.execute(final_function_sql)
			logging.debug(final_function_sql)
		except Exception as e:
			logging.error(e)
			logging.debug(final_function_sql)
			logging.info(
				'Problem occured at inserting data into function table.')
		con.commit()
		cursor = con.cursor()

		try:
			str_final_updates = 'UPDATE ab_user '\
					    'SET coverage_id = c.id, function_id = f.id '\
					    'FROM ab_coverage c, ab_function f, '\
					    'temp_update_user '\
					    'WHERE c.name = temp_update_user.coverage '\
					    'AND f.name = temp_update_user.function '\
					    'AND ab_user.email = temp_update_user.email'
			cursor.execute(str_final_updates)
			logging.debug(str_final_updates)
		except Exception as e:
			logging.error(e)
			logging.info('Problem occured at updating ab_user')
		con.commit()
		cursor = con.cursor()

		# Remove temp tables when update is successful
		try:
			str_drop_tuu = 'DROP TABLE temp_update_user'
			cursor.execute(str_drop_tuu)
			logging.debug(str_drop_tuu)

			str_drop_tbu = 'DROP TABLE temp_backup_user'
			cursor.execute(str_drop_tbu)
			logging.debug(str_drop_tbu)
		except Exception as e:
			logging.error(e)
			logging.info('Problem occured at dropping temp tables.')

		con.commit()
	except Exception as e:
		logging.error(e)
	con.close()

if __name__ == '__main__':
	main()
