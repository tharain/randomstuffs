import $ from 'jquery';

$(document).keydown((key) => {
	if(key.ctrlKey & key.which === 13) {
		this.runQuery(this.props.database.allow_run_async);
	}
});

const runQueryTip = (
	<Tooltip id="runQueryTip">
		<td>
			Tip: hotkeys `ctrl` + `Enter` to Run Query.
		</td>
	</Tooltip>
);

<OverlayTrigger placement="left" overlay={runQueryTip}>
</OverlayTrigger>
